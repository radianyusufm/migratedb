
@extends('master.master')

  @section('content')
  <a href="{{route('pertanyaan.create')}}" class="btn btn-primary">Buat pertanyaan  </a>


    <div class="row">
      @php $no = 1 @endphp
      @forelse($pertanyaan as $item)
          <div class="col-sm-5">
            <h1>pertanyaan {{ $no++ }} </h1>
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">{{$item->judul}}</h5>
                <p class="card-text">{{$item->isi}}</p>
                <p>Penanya :{{$item->user->name}}</p>
                <a href="/pertanyaan/{{$item->id}}" class="btn btn-info">Detail </a>

                @if ($item->user->name == $user_sekarang)
                  <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-warning">Edit </a>

                  <form action="/pertanyaan/{{$item->id}}" method="post" style="display:inline-block">
                    @method('DELETE')
                    @csrf
                    <input type="submit"  class="btn btn-danger" value="Delete">
                  </form>
                @endif

              </div>
            </div>
          </div>

      @empty

        <p>Data kosong</p>

      @endforelse

    </div>
@endsection
