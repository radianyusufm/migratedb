@extends('master.master')

@section('content')
    <div>

        <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
          @method('PUT')
          @csrf


            <label for="judul">Judul</label>
        <input type="text" name="judul" value="{{$pertanyaan->judul}}">

            <label for="isi">Isi</label>
            <input type="text" name="isi" value="{{$pertanyaan->isi}}">

            <input type="submit" value="kirim">

        </form>
    </div>
