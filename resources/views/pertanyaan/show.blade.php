@extends('master.master')

@section('content')

<a href="/pertanyaan" class="btn btn-info"> << Kembali </a>
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">{{ $pertanyaan -> judul}}</h1>
        <p class="lead">{{$pertanyaan -> isi }}</p>
        <p class="lead">Author : {{$pertanyaan -> user ->name }}</p>
      </div>
    </div>



    <form>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Jawaban</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
      </div>

        <div class="form-group">
          <a href="/pertanyaan" class="btn btn-info"> Jawab </a>
        </div>
    </form>
@endsection
