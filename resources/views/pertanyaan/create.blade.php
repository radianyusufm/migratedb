@extends('master.master')

@section('content')

    <h1>Buat pertanyaan</h1>


    <div>
        <form action="{{route('pertanyaan.index')}}" method="post">

            @csrf
            <div class="form-group row">
              <label for="judul" class="col-sm-2 col-form-label" >Judul pertanyaan : </label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="judul" value="">
                </div>
            </div>

            <div class="form-group row">
              <label for="isi" class="col-sm-2 col-form-label" >Isi pertanyaan : </label>
                <div class="col-sm-6">
              <input type="text" class="form-control" name="isi" value="" >
            </div>
            </div>
            <div class=" form-group row">
               <div class="col-sm-6">
                 <button type="submit" class="btn btn-primary">Tanya </button>
               </div>
             </div>
        </form>

@endsection
