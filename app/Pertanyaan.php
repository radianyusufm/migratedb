<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $guarded = [];

    public function user()
     {
         return $this->belongsTo('App\User', 'user_id');
     }

     public function jawaban(){
       return $this->hasOne('App\Jawaban', 'jawaban_tepat_id');
     }
}
