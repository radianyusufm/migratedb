<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;



class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //menampilkan semua

        $pertanyaan = Pertanyaan::all();
        $user_sekarang = Auth::user()->name;


        return view('pertanyaan.index', compact("pertanyaan", "user_sekarang"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //menampilkan form
        return view('pertanyaan.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //memasukan inputan form ke database
        $pertanyaan = Pertanyaan::create([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'user_id' => Auth::id()
          ]);



        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function show(Pertanyaan $pertanyaan)
    {
        //menampilkan satu data berdasarkan id
        $pertanyaan = Pertanyaan::find($pertanyaan->id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pertanyaan $pertanyaan)
    {
        //mengambil data berdasarkan id
        $pertanyaan = Pertanyaan::find($pertanyaan->id);

        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pertanyaan $pertanyaan)
    {
        //mengupdate data
        // $request = inputan dari user
        // $pertanyaan = data yang ada dari database

        $pertanyaan = Pertanyaan::where('id', $pertanyaan->id)->update([
          'judul' => $request['judul'],
          'isi' => $request['isi']
        ]);

        return redirect('/pertanyaan');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pertanyaan $pertanyaan)
    {
        //menghapus berdasarkan
          $pertanyaan = Pertanyaan::destroy($pertanyaan->id);
          return redirect('/pertanyaan');
    }
}
